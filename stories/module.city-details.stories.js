import React from "react";

import { storiesOf } from "@storybook/react";

import { CityDetailsModule } from "../src/modules";
import { CityCard } from "../src/components";

const images = [
  "https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80",
  "https://images.unsplash.com/photo-1562878562-c80950bd5340?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80"
];

const list = [
  {
    title: "Apartment",
    image: images[0]
  },
  {
    title: "Apartment2",
    image: images[1]
  },
  {
    title: "Apartment1",
    image: images[0]
  },
  {
    title: "Apartment5",
    image: images[0]
  }
];

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

storiesOf("Modules/CityDetails", module).add("Default", () => (
  <CityDetailsModule
  	name="Berlin"
  	breadCrumb="Germany › Berlin › Berlin › Properties"
  	description="Berlin, Germany’s capital, dates to the 13th century. Reminders of the city's turbulent 20th-century history include its Holocaust memorial and the Berlin Wall's graffitied remains. Divided during the Cold War, its 18th-century Brandenburg Gate has become a symbol of reunification. The city's also known for its art scene and modern landmarks like the gold-colored, swoop-roofed Berliner Philharmonie, built in 1963."
  >
  	{list.map((item, index) => (
      <CityCard key={index} {...item} />
    ))}
  </CityDetailsModule>
));
