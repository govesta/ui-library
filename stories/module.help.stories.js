import React from "react";

import { storiesOf } from "@storybook/react";

import { HelpModule } from "../src/modules";
import { HelpCard } from "../src/components";

const images = [
  "https://via.placeholder.com/84",
  "https://via.placeholder.com/84"
];
const list = [
  {
    title: "Buy a home",
    subTitle: "With over 1 million+ homes for sale available on the website, Trulia can match you with a house you will want to call home.",
    image: images[0],
    url: "https://goveta.co",
    buttonText:"Find a home",
    buttonColor: "gree"
  },
  {
    title: "See Neighborhoods",
    subTitle: "With more neighborhood insights than any other real estate website, we've captured the color and diversity of communities.",
    image: images[0],
    url: "https://goveta.co",
    buttonText:"Explore Locations",
    buttonColor: "gree"
  },
  {
    title: "Sell a property",
    subTitle: "With over 1 million+ homes for sale available on the website, Trulia can match you with a house you will want to call home.",
    image: images[0],
    url: "https://goveta.co",
    buttonText:"Find an Agent",
    buttonColor: "gree"
  }
];

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

storiesOf("Modules/Help", module).add("Default", () => (
  <HelpModule title="See how Govesta can help">
    {list.map((item, index) => (
      <HelpCard key={index} {...item} />
    ))}
  </HelpModule>
));
