import React from "react";

import { storiesOf } from "@storybook/react";

import { Pagination } from "../src/components";

class PaginationStory extends React.Component {
  state= {
    selected: 1
  }
  onPageChange(selected) {
    console.log(`Story onChange: ${selected}`);
    this.setState({
      selected: selected
    })
  }
  render() {
    return (
      <Pagination
        totalCount={1021}
        pageSize={20}
        marginPagesDisplayed={2}
        pageRangeDisplayed={1}
        forcePage={this.state.selected}
        previousLabel={"Back"}
        nextLabel={"Next"}
        breakLabel={"..."}
        onPageChange={this.onPageChange.bind(this)}
      />
    )
  }

}

storiesOf("Components/Pagination", module).add("Default", () => (
  <PaginationStory />
))

