import React from "react";

import { storiesOf } from "@storybook/react";

import { PropertyDetailsCard } from "../src/components";
import { FeaturedPropertiesModule } from "../src/modules";

const images = [
  "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
];

const list = [
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
    price: "234.000€",
    size: "20",
    sqm: "3456€ / sqm",
    bedrooms: "2",
    bathrooms: "1"
  }
];

storiesOf("Modules/FeaturedProperties", module).add("Default", () => (
  <FeaturedPropertiesModule title="Featured Properties">
    {list.map((item, index) => (
      <PropertyDetailsCard
        {...item}
        key={index}
        carousel
        image={index !== 1 ? [
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
        ] : []}
        linkAs={<a href="https://govesta.co" style={{ display: "inline-block", textDecoration: "none" }} />}
      />
    ))}
  </FeaturedPropertiesModule>
));
