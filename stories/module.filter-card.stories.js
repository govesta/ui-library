import React from "react";

import { storiesOf } from "@storybook/react";

import { FilterCard } from "../src/components";
import { FilterCardModule } from "../src/modules";

const images = [
  "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
];

const list = [
  {
    title: "Apartment",
    subTitle: "One might be your new home ",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "One might be your new home ",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "One might be your new home ",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "One might be your new home ",
    image: images[0],
    url: "https://goveta.co"
  }
];

storiesOf("Modules/FilterCard", module).add("Default", () => (
  <FilterCardModule title="What can we help you find?">
    {list.map((item, index) => (
      <FilterCard key={index} {...item} />
    ))}
  </FilterCardModule>
));
 