import React from "react";

import { storiesOf } from "@storybook/react";

import { GovestaTitle, GovestaText } from "../src/components";


storiesOf("Components/FontsStyles", module).add("Heading", () => (
  <div style={{ width: 550 }}>
    <GovestaTitle
      title="We connect you with local estate agents "
      
    />
  </div>
))
.add("Body Text", () => (
  <div style={{ width: 550 }}>
    <GovestaText
      text="The owner must bear costs that a tenant would not have to."
    />
  </div>

))

