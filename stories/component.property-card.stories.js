import React from "react";

import { storiesOf } from "@storybook/react";

import { PropertyCard } from "../src/components";

const item = {
  title:
    "Vacant two-room Altbau apartment with balcony views towards Viktoriapark in Berlin Kreuzberg",
  location: "Berlin, Kreuzberg",
  features: ["Cheap", "Apartment", "4 rooms", "Balcony"],
  price: "234.000€",
  sqm: "3456€ / sqm"
};

storiesOf("Components/PropertyCard", module)
  .add("Default", () => (
    <div style={{ width: 300 }}>
      <PropertyCard
        {...item}
        carousel
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a href="https://govesta.co" style={{ width: 300, display: "inline-block", textDecoration: "none" }} />}
      />
    </div>
  ))
  .add("Without Carousel", () => (
    <div style={{ width: 300 }}>
      <PropertyCard
        {...item}
        image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        carousel={false}
        linkAs={<a href="https://govesta.co" style={{ width: 300, display: "inline-block", textDecoration: "none" }} />}
      />
    </div>
  ))
  .add("Small on Mobile", () => (
    <div style={{ width: 300 }}>
      <PropertyCard
        {...item}
        smallOnMobile
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a href="https://govesta.co" style={{ width: 300, display: "inline-block", textDecoration: "none" }} />}
      />
    </div>
  )).add("Lightbox", () => (
    <div style={{ width: 300 }}>
      <PropertyCard
        {...item}
        carousel
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        lightbox={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a href="https://govesta.co" style={{ width: 300, display: "inline-block", textDecoration: "none" }} />}
      />
    </div>
  ))
