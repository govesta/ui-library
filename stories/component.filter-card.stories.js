import React from "react";

import { storiesOf } from "@storybook/react";

import { FilterCard } from "../src/components";

storiesOf("Components/FilterCard", module).add("Default", () => (
  <div style={{ width: 308 }}>
    <FilterCard
      title="Apartments"
      subTitle="One might be your new home"
      image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
      url="https://govesta.co"
    />
  </div>
));
