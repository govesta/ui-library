import React from "react";

import { storiesOf } from "@storybook/react";

import { Carousel } from "../src/components";

storiesOf("Components/Carousel", module).add("Default", () => (
  <div style={{ maxWidth: 780 }}>
    <Carousel.Container>
      <Carousel.Item>
        <div
          style={{
            backgroundImage: `url(https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80)`
          }}
        />
      </Carousel.Item>
      <Carousel.Item>
        <div
          style={{
            backgroundImage: `url(https://images.unsplash.com/photo-1562878562-c80950bd5340?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80)`
          }}
        />
      </Carousel.Item>
    </Carousel.Container>
  </div>
));
