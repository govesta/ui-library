import React from "react";

import { storiesOf } from "@storybook/react";

import { Tag } from "../src/components";

storiesOf("Components/Tag", module).add("yellow", () => (
  <Tag color="yellow" content="New" />
)).add("orange", () => (
  <Tag color="orange" content="New" />
))
.add("red", () => (
  <Tag color="red" content="New" />
))
.add("brand-dark", () => (
  <Tag color="brand-dark" content="New" />
))
.add("brand", () => (
  <Tag color="brand" content="New" />
)).add("brand-light", () => (
  <Tag color="brand-light" content="New" />
))

