import React from "react";

import { storiesOf } from "@storybook/react";

import { SeoCard } from "../src/components";

storiesOf("Components/SeoCard", module).add("Default", () => (
  <SeoCard
    title={<span>We connect you with local estate agents</span>}
    text={<span>Over 175 million aggregated hotel ratings and more than 19 million images allow you to find out more about where you're travelling. To get an extended overview of a hotel property, trivago shows the average rating and extensive reviews from other booking sites, e.g. Hotels.com, Expedia, Agoda, leading hotels, etc. trivago makes it easy for you to find information about your weekend trip to Amsterdam, including the ideal hotel for you.</span>}
    
  />
))
