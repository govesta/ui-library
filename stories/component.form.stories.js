import React from "react";

import { storiesOf } from "@storybook/react";

import { UIForm } from "../src/components";

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' }
]

storiesOf("Components/Form", module)
  .add("Input", () => (
    <UIForm.Field label="First Name">
      <UIForm.Input placeholder="Your Add Here..." />
    </UIForm.Field>
  )).add("Input Numeric", () => (
    <UIForm.Field label="First Name">
      <UIForm.Input numeric value={11} placeholder="Your Add Here..." suffix="SQM" onChange={(value) => { console.log(value) }} />
    </UIForm.Field>
  )).add("Input with Suffix", () => (
    <UIForm.Field label="First Name">
      <UIForm.Input placeholder="Your Add Here..." suffix="SQM" />
    </UIForm.Field>
  )).add("Input Error", () => (
    <UIForm.Field label="First Name" error="*Required">
      <UIForm.Input placeholder="Your Add Here..." suffix="SQM" />
    </UIForm.Field>
  )).add("TextArea", () => (
    <UIForm.Field label="Description">
      <UIForm.TextArea placeholder="Your Description Here..." />
    </UIForm.Field>
  )).add("TextArea Error", () => (
    <UIForm.Field label="Description" error="*Required">
      <UIForm.TextArea placeholder="Your Description Here..." />
    </UIForm.Field>
  )).add("Select", () => (
    <UIForm.Field label="Options">
      <UIForm.Select value="vanilla" placeholder="Select a Option" options={options} onChange={(value) => { console.log(value) }} />
    </UIForm.Field>
  )).add("Select Error", () => (
    <UIForm.Field label="Options" error="*Required">
      <UIForm.Select value="vanilla" placeholder="Select a Option" options={options} onChange={(value) => { console.log(value) }} />
    </UIForm.Field>
  )).add("CheckBox", () => (
    <UIForm.CheckBox>
      <span>Price on Request</span>
    </UIForm.CheckBox>
  ))
  .add("PhoneInput", () => {
    class StoryComp extends React.Component {
       constructor( props ){
         super(props);
 
         this.state = {
           countries : [],
         }
       }
 
       async wait(ms) {
         // mocking an API call
         return new Promise(resolve => {
           setTimeout(resolve, ms);
         });
       }
 
       async componentDidMount() {
         await this.wait(1000);
 
         this.setState({ countries: [
          {
            "id": 1,
            "featured_image_id": null,
            "code": "AF",
            "code3": "AFG",
            "currency": null,
            "phone_prefix": 93,
            "property_published_count": 0,
            "status": 0,
            "name": "Afghanistan",
            "slug": "afghanistan",
            "description": null,
            "translations": [
                {
                    "id": 1,
                    "country_id": 1,
                    "name": "Afghanistan",
                    "slug": "afghanistan",
                    "description": null,
                    "locale": "en"
                }
            ]
          },
          {
              "id": 2,
              "featured_image_id": null,
              "code": "AL",
              "code3": "ALB",
              "currency": null,
              "phone_prefix": 355,
              "property_published_count": 0,
              "status": 0,
              "name": "Albania",
              "slug": "albania",
              "description": null,
              "translations": [
                  {
                      "id": 2,
                      "country_id": 2,
                      "name": "Albania",
                      "slug": "albania",
                      "description": null,
                      "locale": "en"
                  }
              ]
          },
          {
              "id": 3,
              "featured_image_id": null,
              "code": "DZ",
              "code3": "DZA",
              "currency": "",
              "phone_prefix": 213,
              "property_published_count": 0,
              "status": 0,
              "name": "Algeria",
              "slug": "algeria",
              "description": null,
              "translations": [
                  {
                      "id": 3,
                      "country_id": 3,
                      "name": "Algeria",
                      "slug": "algeria",
                      "description": null,
                      "locale": "en"
                  }
              ]
          },
          {
              "id": 4,
              "featured_image_id": null,
              "code": "AS",
              "code3": "ASM",
              "currency": null,
              "phone_prefix": 1684,
              "property_published_count": 0,
              "status": 0,
              "name": "American Samoa",
              "slug": "american-samoa",
              "description": null,
              "translations": [
                  {
                      "id": 4,
                      "country_id": 4,
                      "name": "American Samoa",
                      "slug": "american-samoa",
                      "description": null,
                      "locale": "en"
                  }
              ]
          }
           ]
         });
       }
 
       render() {
         const { countries, countryCode, phone } = this.state;
         return (
          <div style={{width: '296px'}}>
            <UIForm.PhoneInput
              placeholder="Phone"
              options={countries}
              defaultCountryCode="0093"
              defaultPhoneNumber="1234"
              onChange={(value) => console.log(value)}
            />
          </div>
         )
       }
     }
     return <StoryComp />
 });