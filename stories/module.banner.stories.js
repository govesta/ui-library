import React from "react";

import { storiesOf } from "@storybook/react";

import { BannerModule } from "../src/modules";

const images = [
  "https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80",
  "https://images.unsplash.com/photo-1562878562-c80950bd5340?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80"
];

storiesOf("Modules/Banner", module)
  .add("Default", () => (
    <BannerModule
      image={images[0]}
      smallText={<span>Introducing</span>}
      title={<span>Govesta property search </span>}
      description={
        <span>
          Hosted journeys to extraordinary places. All you have to do is show
          up.
        </span>
      }
      linkAs={<a href="/atilla" />}
      linkText="Learn more"
    />
  ))
  .add("Full", () => (
    <BannerModule
      full
      image={images[0]}
      smallText={<span>Introducing</span>}
      title={<span>Govesta property search </span>}
      description={
        <span>
          Hosted journeys to extraordinary places. All you have to do is show
          up.
        </span>
      }
      linkAs={<a href="/atilla" />}
      linkText="Learn more"
    />
  ))
  .add("Dark", () => (
    <BannerModule
      dark
      image={images[0]}
      smallText={<span>Introducing</span>}
      title={<span>Govesta property search </span>}
      description={
        <span>
          Hosted journeys to extraordinary places. All you have to do is show
          up.
        </span>
      }
      linkAs={<a href="/atilla" />}
      linkText="Learn more"
    />
  ));
