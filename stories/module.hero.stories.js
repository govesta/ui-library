import React from "react";

import { storiesOf } from "@storybook/react";

import { HeroModule } from "../src/modules";

const images = [
  "https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80",
  "https://images.unsplash.com/photo-1562878562-c80950bd5340?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80"
];

storiesOf("Modules/Hero", module).add("Default", () => {
   class StoryComp extends React.Component {
      constructor( props ){
        super(props);

        this.state = {
          options : [
            { value: "chocolate", label: "Chocolate" },
            { value: "strawberry", label: "Strawberry" },
            { value: "vanilla", label: "Vanilla" }
          ],
          isLoading: false,
          defaultValue: null
        }
      }

      async wait(ms) {
        // mocking an API call
        return new Promise(resolve => {
          setTimeout(resolve, ms);
        });
      }

      async onChange(value) {
        if(!value || value.length < 1) {
          this.setState({ options: [], isLoading: false, defaultValue: null });
          return
        }
        this.setState({ options: [], isLoading: true, defaultValue: null });

        await this.wait(1000);

        this.setState({ options: [
            { value: "chocolate", label: "Chocolate" },
            { value: "strawberry", label: "Strawberry" },
            { value: "vanilla", label: "Vanilla" }
          ],
          isLoading: false
        });
      }

      async onBlur() {
        if((!this.state.defaultValue || this.state.defaultValue.label.length < 1)  && this.state.options && this.state.options.length > 0)
          this.setState({ defaultValue: this.state.options[0]});
      }

      onSelect(value) {
        this.setState({ defaultValue: value});
        this.setState({options: []});
      }

      render() {
        return (
        <HeroModule
          images={images}
          notification={{
            tag: "New",
            text: "You can search for <b>Neighborhoods</b> now"
          }}
          slogan="There’s no place like home.<br />Find your dream home."
          history={{
            title: "Popular searches",
            items: [
              { label: "Kreuzberg", value: "kreuzberg" },
              { label: "Berlin", value: "berlin" },
              { label: "London", value: "london" }
            ],
            onClick: value => {
              console.log(value);
            }
          }}
          autocomplete={{
            onChange: this.onChange.bind(this),
            onBlur: this.onBlur.bind(this),
            options: this.state.options,
            isLoading:this.state.isLoading,
            placeholder: "Kreuzberg, Berlin",
            indicatorIcon: "icon-search",
            onSelect: this.onSelect.bind(this),
            defaultValue: this.state.defaultValue
          }}
        />
        )
      }
    }
    return <StoryComp />
});
