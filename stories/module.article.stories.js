import React from "react";

import { storiesOf } from "@storybook/react";

import { ArticleCard } from "../src/components";
import { ArticleModule } from "../src/modules";

const images = [
  "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
];

const list = [
  {
    title: "How much does it cost to maintain a property in Berlin?",
    text: "The monthly fees for an owned apartment are higher than for a rented apartment, as an owner must bear costs that a tenant would not have to.",
    image: images[0],
    tagTitle:'Berlin',
    tagColor:'red',
    url: "https://goveta.co"
  },
  {
    title: "How to get a mortgage in Germany",
    text: "Many expats living in Germany, after a few years or so, find themselves considering a permanent residence. They are interested in transitioni…",
    image: images[0],
    tagTitle:'Berlin',
    tagColor:'red',
    url: "https://goveta.co"
  },
  {
    title: "Berlin Property Price development in 2020",
    text: "Property prices have recently increased a lot in Berlin. But compared to other cities, it’s still very affordable. So what’s the market like in 2020?",
    image: images[0],
     tagTitle:'Berlin',
    tagColor:'red',
    url: "https://goveta.co"
  },
  {
    title: "A guide to visiting apartments in Germany",
    text: "First-time property buyers in Germany, whether ex-pats or German natives, face the uncharted waters of their first real estate experience. The ... ",
    image: images[0],
    tagTitle:'Berlin',
    tagColor:'red',
    url: "https://goveta.co"
  },
  // {
  //   title: "How much does it cost to maintain a property in Berlin?",
  //   text: "The monthly fees for an owned apartment are higher than for a rented apartment, as an owner must bear costs that a tenant would not have to.",
  //   image: images[0],
  //   url: "https://goveta.co"
  // },
  // {
  //   title: "Apartment",
  //   text: "The monthly fees for an owned apartment are higher than for a rented apartment, as an owner must bear costs that a tenant would not have to.",
  //   image: images[0],
  //   url: "https://goveta.co"
  // },
  // {
  //   title: "How much does it cost to maintain a property in Berlin?",
  //   text: "The monthly fees for an owned apartment are higher than for a rented apartment, as an owner must bear costs that a tenant would not have to.",
  //   image: images[0],
  //   url: "https://goveta.co"
  // },
  // {
  //   title: "How much does it cost to maintain a property in Berlin?",
  //   text: "The monthly fees for an owned apartment are higher than for a rented apartment, as an owner must bear costs that a tenant would not have to.",
  //   image: images[0],
  //   url: "https://goveta.co"
  // }
];

storiesOf("Modules/Article", module).add("Default", () => (
  <ArticleModule title="Popular articles" subTitle="All articles">
    {list.map((item, index) => (
      <ArticleCard key={index} {...item} />
    ))}
  </ArticleModule>
));
