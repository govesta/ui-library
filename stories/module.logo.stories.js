import React from "react";

import { storiesOf } from "@storybook/react";

import { LogoModule } from "../src/modules";

const images = [
  "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
];

storiesOf("Modules/Logo", module).add("Default", () => (
  <LogoModule>
    <img src={require('./logo.png')} />
    <img src={require('./logo.png')} />
    <img src={require('./logo.png')} />
    <img src={require('./logo.png')} />
    <img src={require('./logo.png')} />
  </LogoModule>
));
