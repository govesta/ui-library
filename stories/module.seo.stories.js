import React from "react";

import { storiesOf } from "@storybook/react";

import { SeoCard } from "../src/components";
import { SeoModule } from "../src/modules";

const images = [
  "https://www.countryflags.io/at/flat/64.png"
];

const list = [
  {
    title: "Govesta the global property search",
    text: "Govesta's property search connects users to top real estate agents around the world in just a few clicks. Many of our users are interested in oversees property. Check out some of the top destinations in Europe, like Berlin, Barcelona, Lisbon and Paris. On Govesta, you'll find real estate agents that speak your language. Get the best market knowledge and local support with your property search by contacting the trusted agents on our site.",
    url: "https://govesta.co"
  },
  {
    title: "Find your ideal home on Govesta",
    text: "With Govesta you can easily find your new home. Simply enter your ideal location and let our property search engine connect you with the best local and international real estate agents. To refine your search results, simply filter by price, m2, facilities and more. From budget holiday homes to luxury penthouses, Govesta makes it easy to start your property search online.",
    url: "https://govesta.co"
  },
  {
    title: "We connect you with local estate agents ",
    text: "Our journal provides you with in-depth market knowledge to help you become a local property expert and discover more about where you're buying. Our search results give you the most important details of the property, such as price per m2. But we also recommend to get in touch with the agents listed on Govesta. We are here to connect you with the experts. Our trusted real estate partners have the best local knowledge and will help you with your property search. ",
    url: "https://govesta.co"
  },
  {
    title: "How to buy a property",
    text: "Govesta is a property search that connects you to property sellers around the world. The properties shown come from the websites of top real estate agents. This means that while users decide on Govesta which property best suits their needs, the purchasing process itself is started on the real estate agent's site. By clicking on the “View Deal” button, you will be forwarded onto the seller's website where you can contact them about the property found on Govesta.",
    url: "https://govesta.co"
  },
  
 
];

storiesOf("Modules/Seo", module).add("Default", () => (
  <SeoModule>
    {list.map((item, index) => (
      <SeoCard key={index} {...item} />
    ))}
  </SeoModule>
));
