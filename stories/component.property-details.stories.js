import React from "react";

import { storiesOf } from "@storybook/react";

import { PropertyDetails, PropertyDetailsCard } from "../src/components";
const lat = 52.518080;
const lng = 13.314350;
const location = "Charlottenburg, Berlin, Berlin, Germany";

const item = {
  title:
    "Vacant two-room Altbau apartment with balcony views towards Viktoriapark in Berlin Kreuzberg",
  location: "Berlin, Kreuzberg",
  description: `2-room-apartment in exceptional location – Choriner Straße: Balcony and great views!

  This 2-room-apartment is located on the 4th floor of the sidewing of a period building. The building is listed as part of the ensemble “Teutoburger Platz”. The beautiful historic building has been refurbished in 2006. The refurbishment included: 
  
  -Complete refurbishment of the facades 
  -New rain water pipes etc. 
  -Check of all wooden structures of the roof
  -Replacement of the old roof structure
  -Remodeling of the yard
  -Complete refurbishment of pipes inside the building
  -Installation of a new gas central heating 
  -Complete refurbishment of the stairwells
  -Complete refurbishment of the basement 
  
  The apartment has also been refurbished in 2006. The apartment has a balcony, wooden parquet and wooden insulated windows. Furthermore a new bathroom and a built-in kitchen have been built. 
  
  Above all, what will catch your attention is its modern and efficient layout. The large living room combines living and kitchen and is thus the heart of the apartment. The built-in kitchen is included in the purchase price. The open views from the living room are extremely pleasant and the balcony invites for a cozy breakfast in the morning sun! 
  
  The bedroom offers space for a large bed, closet and a table. The bright bathroom with window is tiled with mosaic and because of its warm Orange color inviting and cozy. It offers a tub with shower, basin, toilet and connection for the washing machine.`,
  features: [{title: "Cheap"}, {title: "Apartment"}, {title: "4 rooms"}, {title: "Balcony"}],
  price: "234.000€",
  sqm: "3456€ / sqm",
  mapsApi: "AIzaSyCS4-oEErbzd_k6q-wi0bkKDrePWyOCN-8",
  featuresList:[{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
  navIcons: {close: '', next: '', prev: ''},
};

const cardItem = {
  title:"Berlin, Steglitz · Villa · Garden · Pool Berlin, Steglitz · Villa · Garden · Pool",
  location: "Berlin, Kreuzberg Berlin, Kreuzberg Berlin, Kreuzberg",
  features: [{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
  price: "234.000€",
  size: "106.9",
  sqm: "3456€ / sqm",
  bedrooms: "2",
  bathrooms: "1"
};

storiesOf("Components/PropertyDetails", module)
  .add("Default", () => (
    <div>
      <PropertyDetails
        enableSlide
        {...item}
        lat={lat}
        lng={lng}
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a className="property-details__link" href="https://govesta.co" target="_blank"/>}
      />
    </div>
  ))
  .add("Without lat & long", () => (
    <div>
      <PropertyDetails
        {...item}
        enableSlide
        location={location}
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a className="property-details__link" href="https://govesta.co" target="_blank"/>}
      />
    </div>
  ))
  .add("With navigate", () => (
    <div>
      <PropertyDetails
        {...item}
        location={location}
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a href="https://govesta.co" style={{ display: "inline-block", textDecoration: "none" }} />}
      />
    </div>
  ))
  .add("PropertyDetailCard", () => (
    <div style={{ width: 300 }}>
      <PropertyDetailsCard
        {...cardItem}
        enableSlide
        carousel
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          // "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          // "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          // "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a href="https://govesta.co" style={{ width: 300, display: "inline-block", textDecoration: "none" }} />}
      />
    </div>
  ))
  .add("PropertyDetailCard With navigate", () => (
    <div style={{ width: 300 }}>
      <PropertyDetailsCard
        style={{ width: 300, display: "inline-block", textDecoration: "none" }}
        {...cardItem}
        carousel
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          // "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          // "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          // "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        redirectToAgency={false}
        onNavigate={() => alert('on navigate')}
      />
    </div>
  ));
