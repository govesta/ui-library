// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string
  subTitle?: string
}

export { IProps as IFullCardProps }

const FullCard = ({ title, subTitle, image }: IProps) => {
  return (
    <div className="g-full-card">
      <div>
        <div className="g-full-card__image" style={{ backgroundImage: `url(${image})` }} />
        <div className="g-full-card__content">
          {title && <span className="g-full-card__title">{title}</span>}
          {subTitle && <span className="g-full-card__subtitle">{subTitle}</span>}
        </div>
      </div>
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

FullCard.defaultProps = defaultProps;

export default FullCard;