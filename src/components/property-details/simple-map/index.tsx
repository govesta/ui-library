import * as React from 'react';
import GoogleMapReact from 'google-map-react';

interface IProps {
  location?: string
  centerLng?: string
  centerLat?: string
  markerLng?: string
  markerLat?: string
  mapsApi?: string
}

class SimpleMap extends React.Component<IProps> {
  
  constructor(props: IProps) {
    super(props);
  }
  
  getMapOptions (maps: any) {
    return {
        disableDefaultUI: true,
        mapTypeControl: false,
        streetViewControl: false,
        zoomControl: false
    };
  };

  render() {
    const GoogleMapsMarker = ({e}: any) => <div className="g-property-marker"></div>;
    return (
      <div className="g-property-map">
        <GoogleMapReact
            bootstrapURLKeys={{ key: this.props.mapsApi }}
            defaultCenter={{lat: +this.props.centerLat, lng: +this.props.centerLng }}
            defaultZoom={12}
            options={this.getMapOptions}
        >
            <GoogleMapsMarker
                lat={+this.props.markerLat}
                lng={+this.props.markerLng}
            />
        </GoogleMapReact>
      </div>
    );
  }
}

export default SimpleMap
