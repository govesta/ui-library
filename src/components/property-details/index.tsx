// Libraries
import * as React from 'react';
import SimpleMap from './simple-map';
import GeoHelper from "../../helpers/geoHelper"

// Style
import './style.styl'

interface IProps {
    linkAs?: any
    title?: string
    location?: string
    features?: Array<any>
    featuresList?: Array<any>
    description?: string
    navIcons?: any
    lng?: string
    lat?: string
    image: Array<any>
    price?: string
    sqm?: string
    mapsApi?: string
    onNavigate?: any
    enableSlide?: boolean
}

interface IPropertyDetailsState {
    current: number
    activeImage: number
    expanded: boolean
    selectedTab: number,
    mapData?: IMapData,
    description?: any
}

interface IMapData {
    markerLng?: string
    markerLat?: string
    centerLng?: string
    centeLat?: string
}

class PropertyDetails extends React.Component<IProps, IPropertyDetailsState> {
    public static defaultProps = {
        enableSlide: false
    };

    state = {
        current: 0,
        activeImage: 0,
        expanded: false,
        selectedTab: 1,
        mapData: {
            markerLng: "",
            markerLat: "",
            centerLng: "",
            centerLat: ""
        },
        description: ""
    }

    constructor(props: IProps) {
        super(props)
    }

    async componentDidUpdate() {
        if(this.state.selectedTab === 2 &&
            (this.state.mapData.markerLng.length === 0
            || this.state.mapData.markerLat.length === 0
            || this.state.mapData.centerLng.length === 0
            || this.state.mapData.centerLat.length === 0)) {
            const info = await GeoHelper.getLatAndLong(this.props.lat, 
                this.props.lng, 
                this.props.location, 
                this.props.mapsApi);
            this.setState({
                mapData: info
            });
        } else if (this.state.selectedTab === 1 
            && this.state.description === "")
                this.putLineBreakOnDescription();
    }

    putLineBreakOnDescription() {
        if(!this.props.description || this.props.description.length ===0) return;
        const lines = this.props.description.split('\n');
        const newDesc = lines.map((line, index) => (
            <div key={index}>{line}</div>
        ));
        this.setState({
            description: newDesc
        })
    }

    onPrev = (e: any): void => {
        if (this.state.activeImage > 0) {
            this.setState({activeImage: this.state.activeImage - 1})
        }
    }

    onNext = (e: any): void => {
        if (this.state.activeImage < this.props.image.length - 1) {
            this.setState({activeImage: this.state.activeImage + 1})
        }
    }

    hideExpanded = (e: any): void => {
        this.setState( {expanded: false})
        this.setState({selectedTab: 0})
    }

    toggleExpanded = (tabIndex: number): void => {
        if(!this.props.enableSlide) return;
        if (this.state.expanded && this.state.selectedTab === tabIndex) {
            this.hideExpanded('');
        } else {
            this.setState({expanded: true});
            this.setState({selectedTab: tabIndex})
        }
    }

    expandOrNavigate = (tabIndex: number): void => {
        this.toggleExpanded(tabIndex);
    }

    setTab = (tabIndex: number): void => {
        this.setState({expanded: true});
        this.setState({selectedTab: tabIndex})
    }

    render() {
        const {current, expanded, selectedTab} = this.state;
        const { enableSlide } = this.props;
        const tabs = [
            { title: 'Image Gallery'},
            { title: 'Property Infos'},
            { title: 'Location'},
            ]
        const cardTabs = (
            <React.Fragment>
                <div className="g-property-tabs">
                    <div className="g-property-tabs__close" onClick={this.hideExpanded}><img src={this.props.navIcons.close}/></div>
                    {tabs.map((tab, tabIndex) => (
                        <div className={"g-property-tabs__tab " + (selectedTab === tabIndex && "g-property-tabs__tab--active")} onClick={() => this.setTab(tabIndex)}>
                            {tab.title}
                        </div>
                    ))}
                </div>
            </React.Fragment>
        )

        const cardExpandedContent = (

            <div className="g-property-expanded-content">
                { selectedTab === 0 && (
                    <div>
                        <div className="g-property-expanded-content__section">
                            {
                                <div className="g-property-gallery" style={
                                    {backgroundImage: `url(${this.props.image[this.state.activeImage]})`}
                                }>
                                    { this.state.activeImage !== 0 && <div className="g-property-gallery__navigation g-property-gallery__prev" onClick={this.onPrev}><img src={this.props.navIcons.prev}/></div> }
                                    { this.state.activeImage !== this.props.image.length - 1 && <div className="g-property-gallery__navigation g-property-gallery__next" onClick={this.onNext}><img src={this.props.navIcons.next}/></div> }
                                    { this.props.image.length && <div className="g-property-gallery__state"> { this.state.activeImage + 1 } of { this.props.image.length }</div>}
                                </div>
                            }
                        </div>
                    </div>
                )}
                { selectedTab === 1 && (
                <div>
                    {Array.isArray(this.props.featuresList) && Boolean(this.props.featuresList.length) && <div className="g-property-expanded-content__section">
                        <div className="g-property-expanded-content__title">FEATURES</div>
                        <div className="g-property-expanded-content__content">
                            {Array.isArray(this.props.featuresList) && this.props.featuresList.map(feature => (
                                <div className="g-property-expanded-content__cell">
                                    <img className="g-property-expanded-content__feature-icon" src={feature.icon}/>
                                    <span className="g-property-expanded-content__feature-title">{ feature.title }</span>
                                </div>
                            ))}
                        </div>
                    </div>}
                    {this.props.description && <div className="g-property-expanded-content__section">
                        <div className="g-property-expanded-content__title">DESCRIPTION</div>
                        <span
                            role="textbox"
                            className="textarea g-property-expanded-content__content">
                                {this.state.description}
                        </span >
                    </div>}
                </div>
                )}
                { selectedTab === 2 && (
                    this.state.mapData.centerLat && this.state.mapData.centerLng ? <div>
                        <div className="g-property-expanded-content__section">
                            <span className="g-property-expanded-content__location">{this.props.location}</span>
                            <SimpleMap 
                                mapsApi={this.props.mapsApi} 
                                centerLat={this.state.mapData.centerLat} 
                                centerLng={this.state.mapData.centerLng} 
                                markerLat={this.state.mapData.markerLat} 
                                markerLng={this.state.mapData.markerLng}
                                location={this.props.location}/>
                        </div>
                    </div> : null
                )}
                <div className="g-property-info">
                    <div className="g-property-info__text">The Information displayed on Govesta is provided by and proprietary to third parties.</div>
                    <button className="g-property-info__btn" onClick={this.hideExpanded}>Close</button>
                </div>
            </div>
        )
        const cardExpanded = (
            <React.Fragment>
                <div className="g-property-details__expanded">
                    {cardTabs}
                    {cardExpandedContent}
                </div>
            </React.Fragment>
        )
        const cardImage = (<div className={`g-property-details__image`}
                                onClick={() => this.expandOrNavigate(0)}
                                style={{backgroundImage: `url(${Array.isArray(this.props.image) ? this.props.image[0] : this.props.image})`}}/>);
        const featuresElement = this.props.features.map(feature =>
            <div className="g-property-features__feature">
                <img className="g-property-features__icon" src={feature.icon}/>
                <span className="g-property-features__title">
                  {feature.title}
                </span>
            </div>);
        const cardFeatures = (
            <React.Fragment>
                <div>
                    <div className="g-property-features" onClick={() => this.expandOrNavigate(1)}>
                        {featuresElement}
                    </div>
                </div>
            </React.Fragment>
        )
        const cardContent = (
            <React.Fragment>
                {this.props.title && <span className="g-property-details__title" onClick={() => this.expandOrNavigate(1)}>{this.props.title}</span>}
                {this.props.location && <span className="g-property-details__location" onClick={() => this.expandOrNavigate(2)}>{this.props.location}</span>}
                {this.props.features.length != 0 && (
                    cardFeatures
                )}
            </React.Fragment>
        )
        const priceBox = (
            <React.Fragment>
                    <div className="g-property-details__price-sqm">
                        {this.props.price && <span className="g-property-details__price">{this.props.price}</span>}
                        {this.props.sqm && <span className="g-property-details__sqm">{this.props.sqm}</span>}
                    </div>
                    <div className="g-property-details__cta">View Details</div>
            </React.Fragment>)

        const cardSide = enableSlide ? (
            <React.Fragment>
                {
                    React.cloneElement(this.props.linkAs, {
                        className: "g-property-details__side",
                        children: priceBox
                    })
                }
            </React.Fragment>
        ) : <div className="g-property-details__side">{priceBox}</div>

        const wholeComponent = (
            <>
                <div className={`g-property-details`}>
                    <div className="g-property-details__main">
                        {cardImage}
                        <div className="g-property-details__wrapper">
                            <div className="g-property-details__content">
                                {cardContent}
                            </div>
                            {cardSide}
                        </div>
                    </div>
                    {expanded && cardExpanded}
                </div>
            </>
        )

        return enableSlide ? wholeComponent :
        (
            <React.Fragment>
                {
                    React.cloneElement(this.props.linkAs, {
                        children: wholeComponent
                    })
                }
            </React.Fragment>
        )
    }
}

const defaultProps: IProps = {
    image: null,
    features: []
}

export default PropertyDetails;
