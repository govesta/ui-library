// Libraries
import * as React from 'react';
import NumberFormat from 'react-number-format';
import Select from '../select';
import CountryItem from "../../country-item";

// Style
import './style.styl'

interface IProps {
  placeholder?: string
  onChange?: Function
  error?: boolean
  options: any
  defaultCountryCode: string
  defaultPhoneNumber: string
}

interface IState {
  selectedCoutry: any,
  countryCode: string
  phone?: any
}

class PhoneInput extends React.Component<IProps, IState> {
  state = {
    selectedCoutry: {
      value: ""
    },
    countryCode: "",
    phone: ""
  }
  
  onCountryChanged(item: any) {
    this.setState({
      selectedCoutry: item,
      countryCode: item.value
    })

    this.props.onChange({
      countryCode: item.value,
      phone: this.state.phone
    });
  }

  onNumberChanged(value: string) {
    this.setState({
      phone: value
    })

    this.props.onChange({
      countryCode: this.state.countryCode,
      phone: value
    });
  }

  toCountryOption(country: any) {
    if(!country) return;
    return {
      value: "00" + country.phone_prefix,
      label: (
        <div className="g-form-phone-input__country-item">
          <CountryItem
            countryFlag={`https://www.countryflags.io/${country.code.toLowerCase()}/flat/32.png`} /> 
          <span className="country-name">{ country.name }</span>
          <span className="country-name">+{ country.phone_prefix }</span>
        </div>
      )
    }
  }


  render() {
    const {
      options,
      error,
      placeholder,
      defaultCountryCode,
      defaultPhoneNumber
    } = this.props;

    const { phone, selectedCoutry } = this.state;
    const countries = options.map((country: any) => {
      return this.toCountryOption(country);
    });

    if(!selectedCoutry || !selectedCoutry.value || selectedCoutry.value.length < 1) {
      const selectedOption = defaultCountryCode && defaultCountryCode.length > 2 ? 
      this.toCountryOption(options.find(
        (item: any) => item.phone_prefix && item.phone_prefix.toString() === defaultCountryCode.substring(2, defaultCountryCode.length)
        ))
        : null;

      if(selectedOption) {
        this.setState({
          selectedCoutry: selectedOption,
          countryCode: selectedOption.value,
          phone: defaultPhoneNumber
        })
      }
    }
    
    return (
      <div className="g-form-phone-input">
        <Select
          placeholder=""
          className="g-form-phone-input__country-select"
          value={selectedCoutry}
          options={countries}
          onChange={this.onCountryChanged.bind(this)}
        />
        <div className={`g-form-phone-input__number ${error && 'error'}`}>
          <NumberFormat
            placeholder={placeholder}
            value={phone}
            onChange={(e) => this.onNumberChanged(e.target.value)}
          />
        </div>
      </div>
    )
  }
}

export default PhoneInput;