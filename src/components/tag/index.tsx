// Libraries
import * as React from 'react';

// Style
import './style.styl'

type Color = 'yellow' | 'orange' | 'red' | 'brand' | 'brand-dark' | 'brand-light' | 'blue'

interface IProps {
    color?: Color
    content?: any
    className?: string
}

const Tag = ({ content, color, className }: IProps) => {
    return (
        <span className={`g-tag g-tag--${color} ${className}`}>
            {content}
        </span>
    )
}

const defaultProps: IProps = {
    color: 'yellow',
    className: ''
}

Tag.defaultProps = defaultProps;

export default Tag;