// Libraries
import * as React from 'react';
import ReactPaginate from 'react-paginate';

// Style
import './style.styl'

interface IProps {
  totalCount: number,
  pageSize: number,
  marginPagesDisplayed: number,
  pageRangeDisplayed: number,
  onPageChange?: any,
  previousLabel?: string,
  nextLabel?: string,
  breakLabel?: string,
  forcePage: number
}

class Pagination extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  onPageChange(event: any) {
    this.props.onPageChange(event.selected + 1);
  }

  render() {
    const {
      totalCount,
      pageSize,
      previousLabel,
      nextLabel,
      marginPagesDisplayed,
      pageRangeDisplayed,
      breakLabel,
      forcePage
    } = this.props;

    return (
      <ReactPaginate 
        pageCount={totalCount / pageSize}
        containerClassName="g-pagination"
        marginPagesDisplayed={marginPagesDisplayed}
        pageRangeDisplayed={pageRangeDisplayed}
        previousLabel={previousLabel}
        nextLabel={nextLabel}
        breakLabel={breakLabel}
        breakClassName={'break-me'}
        onPageChange={this.onPageChange.bind(this)}
        activeClassName={'active'}
        forcePage={forcePage - 1}
      />
    )
  }
}

export default Pagination;