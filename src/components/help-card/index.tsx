// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string
  subTitle?: string
  buttonText?: string
  buttonColor?: string
  onPress?:any
  className?: string
}

export { IProps as IHelpCardProps }

const HelpCard = ({ title, subTitle, image, buttonText, buttonColor, onPress, className=''}: IProps) => {
  return (
    <div className={`g-help-card ${className}`}>
        <div className="g-help-card__image" style={{ backgroundImage: `url(${image})` }} />
        <div className="g-help-card__content">
        {title && 
          <div className="g-help-card__title">{title}</div>
        }
        {subTitle && 
          <div className="g-help-card__subtitle">{subTitle}</div>
        }
          <div className="g-help-card__buttonwrapper">
            <button className="g-help-card__button">{buttonText}</button>
          </div>
        </div>
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

HelpCard.defaultProps = defaultProps;

export default HelpCard;