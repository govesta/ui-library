// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string
  className?: string
}

export { IProps as ICityCardProps }

const CityCard = ({ title, image, className='' }: IProps) => {
  return (
    <div className={`g-city-card ${className}`}>
        <div className="g-city-card__image" style={{ backgroundImage: `url(${image})` }} />
        
        {title && 
          <div className="g-city-card__title">{title}</div>
        }
        
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

CityCard.defaultProps = defaultProps;

export default CityCard;