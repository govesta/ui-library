// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string
  subTitle?: string
  className?: string
}

export { IProps as ICountryCardProps }

const CountryCard = ({ title, subTitle, image, className='' }: IProps) => {
  return (
    <div className={`g-country-card ${className}`}>
      <div>
        <div className="g-country-card__image" style={{ backgroundImage: `url(${image})` }} />
        <div className="g-country-card__content">
          {title && <span className="g-country-card__title">{title}</span>}
          {subTitle && <span className="g-country-card__subtitle">{subTitle}</span>}
        </div>
      </div>
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

CountryCard.defaultProps = defaultProps;

export default CountryCard;