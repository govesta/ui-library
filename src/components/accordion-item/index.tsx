
// Libraries
import * as React from 'react';

// Style
import './style.styl';

// Interfaces
interface IProps {
  title?: string
  children?: any
  className?: string
}

interface IState {
    open: boolean
}

export { IProps as IAccordionComponentProps }

class AccordionComponet extends React.Component<IProps, IState> {
   state = {
    open: false
   }

   constructor(props: IProps) {
    super(props);
   }
   

  toggle() {
    this.setState({
      open: !this.state.open
    });
  }

  render() {
      const { title, children, className} =  this.props;
    return (
      <div className={`g-accordion-component ${className}`} >
        <button className="g-accordion-component__title" onClick={this.toggle.bind(this)}>
            <span>{title}</span>
            <span className="g-accordion-component__icon"><i className={this.state.open ? 'icon-down' : 'icon-up'}/></span>
        </button>
        <div className={"g-accordion-component__collapse" + (this.state.open ? ' in' : '')}>
            {children}
        </div>
      </div>
    );
  }

};

export default AccordionComponet;
