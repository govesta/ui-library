// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string
  subTitle?: string
}

export { IProps as IFilterCardProps }

const FilterCard = ({ title, subTitle, image }: IProps) => {
  return (
    <div className="g-filter-card">
      <div className="g-filter-card__image" style={{ backgroundImage: `url(${image})` }} />
      <div className="g-filter-card__content">
        <span className="g-filter-card__title">{title}</span>
        <span className="g-filter-card__sub">{subTitle}</span>
      </div>
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

FilterCard.defaultProps = defaultProps;

export default FilterCard;