
const GOOGLE_API = "https://maps.google.com/maps/api/geocode/json";

class GeoHelper {
  getLatAndLong(lat: string, lng: string, location: string, mapsApi: string) {
    return new Promise((resolve, reject) => {
      if(lat && lng) {
        resolve({
          markerLng: lng,
          markerLat: lat,
          centerLng: lng,
          centerLat: lat,
        });
        return;
      }
      this.fromAddress(location, mapsApi, 'en')
      .then(
        (response: any) => {
          const { lat, lng } = response.results[0].geometry.location;
          resolve({
            markerLng: lng,
            markerLat: lat,
            centerLng: lng,
            centerLat: lat,
          });
        }
      ).catch(()=> {
        resolve({
          markerLng: "",
          markerLat: "",
          centerLng: "",
          centerLat: ""
        });
      });
    })
  }

  async fromAddress(address: string, apiKey: string, language: string) {
    if (!address) {
      return Promise.reject(new Error("Provided address is invalid"));
    }

    let url = `${GOOGLE_API}?address=${encodeURIComponent(address)}`;

    if (apiKey) {
      url += `&key=${apiKey}`;
    }

    if (language) {
      url += `&language=${language}`;
    }

    return this.handleUrl(url);
  }

  async handleUrl(url: string) {
    const response = await fetch(url).catch(() =>
      Promise.reject(new Error("Error fetching data"))
    );
  
    const json = await response.json().catch(() => {
      return Promise.reject(new Error("Error parsing server response"));
    });
  
    if (json.status === "OK") {
      return json;
    }
    return Promise.reject(
      new Error(
        `${json.error_message}.\nServer returned status code ${json.status}`
      )
    );
  }

}

export default new GeoHelper();