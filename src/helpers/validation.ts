class ValidationHelper {

    multiple(...validators: any) {
        return ((value: any) => validators.reduce((error: any, validator: any) => error || validator(value), undefined))
    }

}

export default new ValidationHelper();