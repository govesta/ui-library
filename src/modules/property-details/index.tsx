// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import { ModuleTitle } from '../../utils';


interface IProps {
    title?: string
    children?: any
    subTitle?: string
    hasTitle?:boolean
    className?:string
}

interface IState {
    current: number
    loaded: boolean
}

class PropertyDetailsWrapper extends React.Component<IProps, IState> {

    carousel: any = null

    state: IState = {
        current: 0,
        loaded: false
    }

    componentDidMount() {
        this.setState({ loaded: true })
    }

    getItemSize() {
        let size = 4;
        const innerW = window.innerWidth;
        if (innerW < 1200 && innerW > 992) {
            size = 4;
        } else if (innerW < 992 && innerW > 768) {
            size = 3;
        } else if (innerW < 768) {
            size = 2;
        }
        return size;
    }

    goPrexOrNext(isPrev = true) {
        const { current: currentItem } = this.state;
        const size = this.getItemSize();
        const itemW = 100 / size;
        if (this.carousel) {
            if (isPrev) {
                let current = currentItem - 1;
                this.carousel.style = `transform: translateX(-${current * itemW}%)`;
                this.setState({ current })
            } else {
                let current = currentItem + 1;
                this.carousel.style = `transform: translateX(-${current * itemW}%)`;
                this.setState({ current })
            }
        }
    }

    renderPrev() {
        const size = this.getItemSize();
        const { current } = this.state;
        const { children } = this.props;
        const prevActive = children.length > size && current > 0 && size > 2;
        return prevActive && (
            <span className="propertydetailswrapper-module__arrow l" onClick={() => { this.goPrexOrNext(); }}>
                <i className="icon-left"></i>
            </span>
        )
    }

    renderNext() {
        const size = this.getItemSize();
        const { current } = this.state;
        const { children } = this.props;
        const nextActive = children.length > size && ((children.length - size) > current) && size > 2;
        return nextActive && (
            <span className="propertydetailswrapper-module__arrow r" onClick={() => { this.goPrexOrNext(false); }}>
                <i className="icon-right"></i>
            </span>
        )
    }

    render() {
        const { title='', children, subTitle='', hasTitle, className='' } = this.props;
        const { loaded } = this.state;
        return (
            <div className={`propertydetailswrapper-module ${className}`}>
            	{
            		hasTitle &&
            		<ModuleTitle>{title}<span className="g-module-subtitle">{subTitle}</span></ModuleTitle>
            	}
                
                <div className="propertydetailswrapper-module__container">
                    <div className="propertydetailswrapper-module__items">
                        <div className="propertydetailswrapper-module__carousel" ref={(ref) => { this.carousel = ref; }}>
                            {React.Children.map(children, (child, index) => {
                                return (
                                    <div className="propertydetailswrapper-module__item" key={`propertydetails-card-${index}`}>
                                        {child}
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    {loaded && this.renderNext()}
                    {loaded && this.renderPrev()}
                </div>
            </div>
        )
    }

}

export default PropertyDetailsWrapper;