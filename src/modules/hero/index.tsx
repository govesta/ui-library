// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import { Carousel, Button, Tag, Autocomplete } from '../../components';


import IAutocompleteProps from "../../components/autocomplete/IAutocompleteProps"

interface IHistoryItem {
    label: any
    value: any
}

interface IOption {
    label: any
    value: any
}

interface IHistory {
    title: string,
    items: Array<IHistoryItem>
    onClick?: Function
}

interface INotification {
    tag: string,
    text: any
}

interface IProps {
    images: Array<string>,
    notification?: INotification,
    slogan?: any,
    history?: IHistory,  
    autocomplete?: IAutocompleteProps
}


class Hero extends React.Component<IProps> {
    constructor( props: IProps ){
        super(props);
    }

    render() {
        return (
            <div className="hero-module">
                <div className="container">
                    <div className="row middle-xs">
                        <div className="col-md-5 col-sm-6 col-xs-12">
                            {this.props.notification && (
                                <span className="hero-module__notification">
                                    <Tag color="orange" content={this.props.notification.tag} />
                                    <span className="hero-module__notification__text">
                                        {this.props.notification.text}
                                    </span>
                                </span>
                            )}
                            <h1>{this.props.slogan}</h1>
                            {this.props.autocomplete && (
                                <Autocomplete
                                    icon="icon-search"
                                    className="hero-module__autocomplete hero-autocomplete"
                                    placeholder= {this.props.autocomplete.placeholder}
                                    indicatorIcon="icon-search"
                                    onSelect={this.props.autocomplete.onSelect}
                                    onChange= {this.props.autocomplete.onChange}
                                    onBlur= {this.props.autocomplete.onBlur}
                                    options= {this.props.autocomplete.options}
                                    isLoading={this.props.autocomplete.isLoading}
                                    defaultValue= {this.props.autocomplete.defaultValue}
                                />
                            )}
                            {this.props.history.items.length != 0 && (
                                <div className="hero-module__history">
                                    <span className="hero-module__history__title">{this.props.history.title}</span>
                                    {this.props.history.items.map((item) => (
                                        <Button theme="brand-border" key={`history-${item.value}`} onClick={() => { this.props.history.onClick(item.value) }}>{item.label}</Button>
                                    ))}
                                </div>
                            )}
                        </div>
                        <div className="col-md-7 col-sm-6 col-xs-12 last-md last-sm first-xs">
                            <Carousel.Container className="hero-module__carousel">
                                {this.props.images.map((image, index) => (
                                    <Carousel.Item key={`carousel-${index}`}>
                                        <div
                                            style={{
                                                backgroundImage: `url(${image})`
                                            }}
                                        />
                                    </Carousel.Item>
                                ))}
                            </Carousel.Container>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Hero;