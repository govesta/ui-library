// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components

interface IProps {
    title?: any
    children?: any
}

const HelpModule = ({ title, children }: IProps) => {
    return (
        <div className="help-module">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12" >
                        <div className="title">{title}</div>
                        <div className="help-module__cardswrapper">
                            {React.Children.map(children, (child, index) => {
                                return (
                                    <div className="help-module__item" key={`help-card-${index}`}>
                                        {child}
                                    </div>
                                )
                            })}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default HelpModule;