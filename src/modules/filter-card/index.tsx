// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import { ModuleTitle } from '../../utils';


interface IProps {
    title?: any
    children?: any
}

const FilterCard = ({ title, children }: IProps) => {
    return (
        <div className="filtercard-module">
            <ModuleTitle>{title}</ModuleTitle>
            <div className="filtercard-module__container">
                {React.Children.map(children, (child, index) => {
                    return (
                        <div className="filtercard-module__item" key={`full-card-${index}`}>
                            {child}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default FilterCard;