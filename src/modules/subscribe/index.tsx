// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components

interface IProps {
    title?: any
    text?: string
    image?: string
}

const SubscribeModule = ({ title='', text='', image}: IProps) => {
    return (
        <div className="subscribe-module">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12" >
                        <div className="subscribe-module__wrapper">
                            <div className="subscribe-module__infowrapper">
                                <h1 className="subscribe-module__title">{title}</h1>
                                <p className="subscribe-module__text">{text}</p>
                                <form>
                                    <input className="subscribe-module__input" type="text" name="name" />
                                </form>
                            </div>
                            <div className="subscribe-module__image" style={{ backgroundImage: `url(${image})` }} />
                        </div>
                        
                         
                    </div>

                </div>
            </div>
        </div>
    )
}

export default SubscribeModule;