// Libraries
import * as React from 'react';

// Style
import './style.styl'

import { ModuleTitle } from '../../utils';


// Components

interface IProps {
    title?: any
    subTitle?: any
    children?: any
}

const CountryModule = ({ title, subTitle, children  }: IProps) => {
    return (
        <div className="country-module">
            <div className="country-module__title">
                <ModuleTitle >{title}<span className="g-module-subtitle">{subTitle}</span></ModuleTitle>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-xs-12" >
                        <div className="country-module__cardswrapper">
                            {React.Children.map(children, (child, index) => {
                                return (
                                    <div className="country-module__item" key={`country-card-${index}`}>
                                        {child}
                                    </div>
                                )
                            })}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default CountryModule;