// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components

interface IProps {
    title?: any
    children?: any
}

const PropertyTypes = ({ title, children }: IProps) => {
    return (
        <div className="propertytypes-module">
            <div className="container">
                <div className="row">
                	<div className="col-xs-12" >
	                	<div className="title">{title}</div>
                        <div className="propertytypes-module__cardswrapper">
                            {React.Children.map(children, (child, index) => {
                                return (
                                    <div className="propertytypes-module__item" key={`property-card-${index}`}>
                                        {child}
                                    </div>
                                )
                            })}
                        </div>
	                </div>

                </div>
            </div>
        </div>
    )
}

export default PropertyTypes;